#+SETUPFILE: ~/.emacs.d/org-styles/html/stylish_white.theme
#+TITLE: org-multi-clock
#+AUTHOR: Marco Pawłowski
#+EMAIL: pawlowski.marco@gmail.com
#+OPTIONS: num:nil \n:t

* 
org-multi-clock is a small package that extends the functionality of
org-mode clocking system.

It does not change the default behavior, it adds two functions to have
multiple org-clocks in parallel.


** Installation
The straight way is with the following code:
#+begin_src emacs-lisp
(use-package org-multi-clock
  :straight (org-multi-clock :type git :host gitlab :repo "OlMon/org-multi-clock" :branch "master"))
#+end_src

The direct way is to clone this repo into your load-path and require =org-multi-clock=.


** Usage

=org-clock-in= into any task in your org files. To create a parallel clock
use the =omc-make-new-parallel-clock=. This clock will be the /active/ clock.

Active clock is the current clock that is visible to org-mode. The
clock will be displayed in the modeline and clocked out with
=org-clock-out=.

To change the currently /active/ clock use the function =omc-set-active-clock=.

That is it. Not more not less.

*** Maybe a little bit more
While closing an org buffer, it will be checked if there
is a (active or not) clock inside this buffer.

*** Maybe a little bit less
The org functionality of saving the /active/ clock to file, while closing
emacs is not changed and will not work for any other then the /active/ clock.
[Future Work]
